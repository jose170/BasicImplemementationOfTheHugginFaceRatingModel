
'''
Use of bert-base-multilingual-uncased-sentiment pre-driven model for prediction
number of star ratings
'''


#Some package used for our operations
from transformers import AutoTokenizer, AutoModelForSequenceClassification
import torch
import time
from flask import Flask, request, jsonify
from flask_restful import Resource, Api
import json

app = Flask(__name__)
api = Api(app)


tokenizer = AutoTokenizer.from_pretrained("nlptown/bert-base-multilingual-uncased-sentiment")
model = AutoModelForSequenceClassification.from_pretrained("nlptown/bert-base-multilingual-uncased-sentiment")


@app.route('/', methods=['POST'])
def get_review_rating():
	review = json.loads(request.data)
	inputs = tokenizer(review['content'], return_tensors="pt")
	bert_output = model(**inputs).logits.softmax(dim=-1).tolist()[0]

	rating = bert_output[0]*1+bert_output[1]*2+bert_output[2]*3+bert_output[3]*4+bert_output[4]*5
	return jsonify(rating)

if __name__ == '__main__':
     app.run(port='5002')
